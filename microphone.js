(function(ns) {

if (!swfobject.hasFlashPlayerVersion("10.0.0")) {
    if (typeof console != "undefined") {
        console.error("Flash Player >= 10.0.0 is required");
    }
}

// function MicrophoneF() { }
MicrophoneF = function() {
};

function defaultLoader(flash, embedCallback) {
    // var container = document.createElement("div");
    // container.setAttribute("id", "flashMic");
    // container.appendChild(flash);
    // document.body.appendChild(container);

    document.body.appendChild(flash);
    
    embedCallback();
};

var classMethods = {
    enable: function() {
        MicrophoneF.flash.__enable();
    },
    disable: function() {
        MicrophoneF.flash.__disable()
    },
    ondata: function(handler) {
        MicrophoneF.dataHandlers.push(handler);
    },
    onready: function(handler) {
        if (MicrophoneF.flash) {
            handler();
            return;
        }
        // jQuery("#flashMic").css({"z-index":"-10"});
        MicrophoneF.readyHandlers.push(handler);
    },
    initialize: function(options) {
        if (MicrophoneF.flash) return;
        if (!document.body) {
            swfobject.addDomLoadEvent(function() {
                MicrophoneF.initialize(options);
            });
            return;
        }
        options = options || {};
        options["swfLocation"] = options["swfLocation"] || "/MicrophoneMain.swf";
        options["domId"] = options["domId"] || "__microphoneFFlash__";
        options["loader"] = options["loader"] || defaultLoader;

        var flash = document.createElement("div");
        flash.id = options["domId"];

        options["loader"](flash, function() {
            MicrophoneF.flash = flash;
            swfobject.embedSWF(
                options["swfLocation"],
                options["domId"],
                "215",
                "138",
                "10.0.0",
                null,
                { namespace: typeof NS_MICROPHONEF != "undefined"
                    ? NS_MICROPHONEF + ".MicrophoneF"
                    : "MicrophoneF" },
                { hasPriority: true, allowScriptAccess: "always" },
                null,
                function(e) {
                    MicrophoneF.log("Embed " +
                        (e.success ? "succeeded" : "failed"));

                }
            );
        });
    },
    log: function(msg, obj, method) {
        if (!MicrophoneF.debug || !window.console) {
            return;
        }
        method = method || "log";
        console[method]("[MicrophoneF] " + msg);
        if (typeof obj != "undefined") {
            console[method](obj);
        }
    }
};

var flashEventHandlers = {
    __onFlashInitialized: function() {
        MicrophoneF.initialized = true;
        MicrophoneF.flash = document.getElementById(MicrophoneF.flash.id);
        setTimeout(function() {
            MicrophoneF.log("Initialized and ready");
            MicrophoneF.flash.__setDebug(MicrophoneF.debug);
            MicrophoneF.flash.__initialize();
            for (var i = 0; i < MicrophoneF.readyHandlers.length; i++) {
                MicrophoneF.readyHandlers[i]();
            }
        }, 0);
    },
    __onLog: function(msg) {
        MicrophoneF.log(msg);
    },
    __onQueuedSamples: function(framesLength) {
        setTimeout(function() {
            var data = MicrophoneF.flash.__receiveFrames();
            for (var i = 0; i < MicrophoneF.dataHandlers.length; i++) {
                var handler = MicrophoneF.dataHandlers[i];
                handler(data);
            }
        }, 0);
    }
};

for (var name in classMethods) {
    MicrophoneF[name] = classMethods[name];
    // console.log(name);
}

for (var name in flashEventHandlers) {
    MicrophoneF[name] = flashEventHandlers[name];
    // console.log(name);
}

MicrophoneF.debug = false;
MicrophoneF.readyHandlers = [];
MicrophoneF.dataHandlers = [];

ns.MicrophoneF = MicrophoneF;

})(typeof NS_MICROPHONEF != "undefined" ? this[NS_MICROPHONEF] : this);
