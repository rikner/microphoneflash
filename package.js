Package.describe({
    summary: "a js wrapper to get Microphone input from user trough flash."
});

Package.on_use(function(api, where) {
    api.use(['less'], 'client');
    api.add_files(['swfobject.js', 'microphone.js', 'flashObj.less'], 'client');

    if (api.export)
        api.export(['MicrophoneF']);
});

// Package.on_test(function(api) {
//     api.use(['underscore', 'ejson', 'deps', 'settings-manager', 'ui', 'tinytest', 'test-helpers']);
//     api.add_files(['tests/basictests.js'], 'client');
// })